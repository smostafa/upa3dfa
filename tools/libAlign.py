#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 15:26:42 2020

@authors: Mostafa SADEGHI, Sylvain GUY

"""

"""
This code contains different tools for perfroming robust rigid transformation.

"""


import math
import scipy.linalg
import scipy.stats
import numpy as np
from math import  sqrt, log, pi, fabs
from scipy.optimize import  minimize, fsolve
from scipy.stats import t
from sklearn.utils import  check_array
from scipy.special import  psi
from scipy.spatial import distance
import matplotlib.pyplot as plt




"""
    ALIGNMENT
"""


def align(Xtest, Xmodel, maxiter, N, models, inliers, amp=None):
    """
        Compute the different possible alignments
    """
    dic = {}
    for m in models.keys():
        if models[m]["model"] == "cf":
            RCf, tCf, sCf, projCf, reason_crash = alignCf(Xtest,Xmodel,maxiter,N)
            dic[m] = {
                        "R": RCf,
                        "t": tCf,
                        "s": sCf,
                        "proj": projCf,
                        "sig": np.eye(3),
                        "hist_sigma": None,
                        "hist_pi": None,
                        "hist_alpha": None,
                        "prec_out": None,
                        "iter": None,
                        "reason_crash": reason_crash
                    }

        elif models[m]["model"] == "gau":
            RGau,tGau,sGau,projGau,SigGau, hist_sigma, iter, reason_crash = alignGau(Xtest,Xmodel,maxiter,N)
            dic[m] = {
                        "R": RGau,
                        "t": tGau,
                        "s": sGau,
                        "proj": projGau,
                        "sig": SigGau,
                        "hist_sigma": hist_sigma,
                        "hist_pi": None,
                        "hist_alpha": None,
                        "prec_out": None,
                        "iter": iter,
                        "reason_crash": reason_crash
                    }

        elif models[m]["model"] == "gum":
            RGum, tGum, sGum, projGum, SigGum, hist_alpha, hist_pi, hist_sigma, prec_out, iter, reason_crash = alignGum(Xtest, Xmodel, maxiter, N, inliers, amp = models[m]["p"])
            dic[m] = { 
                        "R": RGum,
                        "t": tGum,
                        "s": sGum,
                        "proj": projGum,
                        "sig": SigGum,
                        "hist_sigma": hist_sigma,
                        "hist_pi": hist_pi,
                        "hist_alpha": hist_alpha,
                        "prec_out": prec_out,
                        "iter": iter,
                        "reason_crash": reason_crash
                    }

        elif models[m]["model"] == "gumMostafa":
            R_init, t_init, s_init, projCf, reason_crash = alignCf(Xtest,Xmodel,maxiter,N)
            if not (R_init is None or t_init is None or s_init is None):
                RGum, sGum, SigGum, tGum, hist_alpha, hist_pi, hist_sigma, prec_out, iter, reason_crash = robust_GUM_reg(Xmodel, Xtest, R_init, s_init, t_init, maxiter, inliers)
            else: 
                RGum = None 
                sGum = None
                SigGum = None
                tGum = None 
                hist_alpha = None
                hist_pi = None 
                hist_sigma = None 
                prec_out = None 
                iter = None
            if sGum is None or RGum is None or tGum is None:
               projGum = None 
            else:
               projGum = sGum*RGum.dot(Xtest) + tGum
            dic[m] = {
                        "R": RGum,
                        "t": tGum,
                        "s": sGum,
                        "proj": projGum,
                        "sig": SigGum,
                        "hist_sigma": hist_sigma,
                        "hist_pi": hist_pi,
                        "hist_alpha": hist_alpha,
                        "prec_out": prec_out,
                        "iter": iter,
                        "reason_crash": reason_crash
                    }
        elif models[m]["model"] == "student":
            RStud, tStud, sStud, projStud, SigStud, bidon, hist_sigma, hist_pi, prec_out, reason_crash = alignStud(Xtest, Xmodel, maxiter, N, inliers)
            dic[m] = {
                        "R": RStud,
                        "t": tStud,
                        "s": sStud,
                        "proj": projStud,
                        "sig": SigStud,
                        "hist_sigma": hist_sigma,
                        "hist_pi": None,
                        "hist_alpha": None,
                        "prec_out": prec_out,
                        "iter": None,
                        "reason_crash": reason_crash
                    }

        else:
            assert(False)
    return dic 




def alignCf(Xtest,Xmodel,maxiter,N,verbose=False):
    """
        The standard solution, using quaternion approach
    """
    if verbose: print("CF")
    RCf, tCf, sCf, reason_crash = alignQuatCF(Xmodel,Xtest,checkY=False) #Yn=sRX+ts(X,Y)
    if not(RCf is None or sCf is None or tCf is None):
        RCf = np.asarray(RCf)
        projCf = sCf*RCf.dot(Xtest)+tCf
    else:
        projCf = None
    return RCf, tCf, sCf, projCf, reason_crash




def alignGau(Xtest,Xmodel,maxiter,N,verbose=False):
    """
        The approach with a non isotropic covariance ie mahanalobis distance.
    """
    if verbose: print("Initialization Gau")
    RCf, tCf, sCf, projCf, reason_crash = alignCf(Xtest,Xmodel,maxiter,N)
    if reason_crash is not None:
        return None, None, None, None, None, None, None, reason_crash

    # Standard Gaussian model:
    if verbose: print("Gaussian model")
    RGau, sGau, SigGau, tGau, errsGau, hist_sigma, iter, reason_crash = standard_reg(Xmodel, Xtest, RCf, sCf, tCf, maxiter)
    if not(RGau is None or sGau is None or tGau is None):
        projGau = sGau*RGau.dot(Xtest) + tGau
    else:
        projGau = None
    return RGau, tGau, sGau, projGau, SigGau, hist_sigma, iter, reason_crash




def alignGum(Xtest, Xmodel, maxiter, N, inliers, amp=None, verbose=False):
    """
        The GUM model.
    """
    if verbose: print("Initialization GUM") 
    RCf, tCf, sCf, projCf, reason_crash = alignCf(Xtest, Xmodel, maxiter, N, verbose)
    if (RCf is None or tCf is None or sCf is None):
        return None, None, None, None, None, None, None, None, None, None, reason_crash

    if verbose: print("GUM")
    #Let's choose the right amplitude
    xmin = np.amin(Xmodel[0,:],axis=0)
    ymin = np.amin(Xmodel[1,:],axis=0)
    zmin = np.amin(Xmodel[2,:],axis=0)
    xmax = np.amax(Xmodel[0,:],axis=0)
    ymax = np.amax(Xmodel[1,:],axis=0)
    zmax = np.amax(Xmodel[2,:],axis=0)

    if amp is None:
        amplitude = max([xmax-xmin,ymax-ymin,zmax-zmin])
    else:
        amplitude = amp
    if verbose: print("AMPLITUDE UNIFORM COMPONENT: "+str(amplitude))
    if verbose: print("PI_init=0.5 by default")

    RGum, sGum, SigGum, tGum, hist_alpha, hist_pi, hist_sigma, prec_out, iter, reason_crash = robustGumRegSimplified(Xmodel, Xtest, RCf, sCf, tCf, maxiter, inliers, amplitudeUniform=amplitude, pi=0.5)

    if not(RGum is None or sGum is None or tGum is None):
        projGum = sGum*RGum.dot(Xtest)+tGum
    else:
        projGum = None
    return RGum, tGum, sGum, projGum, SigGum, hist_alpha, hist_pi, hist_sigma, prec_out, iter, reason_crash




def alignStud(Xtest,Xmodel,maxiter,N,inliers,verbose=False):
    """
        The approach with a non isotropic covariance ie mahanalobis distance.
    """
    if verbose: print("Initialization Cf")
    RCf, tCf, sCf, projCf, reason_crash = alignCf(Xtest.copy(),Xmodel,maxiter,N)
    if RCf is None or sCf is None or tCf is None:
        return None, None, None, None, None, None, None, None, None, reason_crash
    

    # Standard Gaussian model:
    if verbose: print("Student model")
    RStud, sStud, SigStud, tStud, w, hist_pi, hist_sigma, prec_out, reason_crash  = robust_Student_reg(Xmodel, Xtest.copy(), RCf, sCf, tCf, maxiter, inliers)
    if not(RStud is None or sStud is None or tStud is None):
        projStud = sStud*RStud.dot(Xtest)+tStud
    else:
        projStud = None
    return RStud, tStud, sStud, projStud, SigStud, (1/w)*np.linalg.norm(SigStud), hist_sigma, hist_pi, prec_out, reason_crash




# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
 
 
# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])





def getDifficulties(s, R, t):
    sd = s
    rx, ry, rz = rotationMatrixToEulerAngles(R)
    rd = rx + ry + rz
    td = np.linalg.norm(t)
    return sd, rd, td






def alignQuatCF(Y,X,w=None,checkY=True):
    """ 
        Closed form solution to align, using quaternions.
    """
    X = X.copy()
    Y = Y.copy()
    D, N = X.shape
    reason_crash = None
    if w is None:
        w = np.ones((1,N))
    if np.allclose(w,np.zeros(w.shape),atol=1e-16):
        reason_crash = "w"
        return None, None, None, reason_crash
    xC,yC,xb,yb = center_datas(X, Y, w, N)
    s = compute_scale(xC, yC, np.eye(3), np.eye(3), w)
    if s is None:
        reason_crash = "s"
        return None, None, None, reason_crash
    M = 0*np.eye(4)
    for i in range(N):
        Mi = (Q(yC[:,i])-s*W(xC[:,i])).T.dot(Q(yC[:,i])-s*W(xC[:,i]))
        M += w[0,i]*Mi
    eVa, eVe = np.linalg.eig(M)
    idx = eVa.argsort()[::-1]   
    eVa = eVa[idx]
    eVe = eVe[:,idx]
    R = rot(eVe[:,-1]/np.linalg.norm(eVe[:,-1]))
    t = yb-s*R.dot(xb)
    return R, t, s, reason_crash



def compute_align_im(X,Y):
    """
        Closed-form alignment using SVD.
    """
    A = np.mat(X)
    B = np.mat(Y)
    A = A.T
    B = B.T
    N = A.shape[0]
    centroid_A = np.mean(A,axis=0)
    centroid_B = np.mean(B,axis=0)
    AA = A-np.tile(centroid_A,(N,1))
    BB = B-np.tile(centroid_B,(N,1))

    # SCALE AND ROTATION ESTIMATION
    num  = 0
    num_ = 0
    den  = 0
    den_ = 0
    for i in range(len(AA)):
        aa = np.array(AA[i].tolist()[0])
        bb = np.array(BB[i].tolist()[0])
        den +=np.dot(aa[0:3],aa[0:3])
        den_ +=np.dot(aa,aa)
        num +=np.dot(bb[0:3],bb[0:3])
        num_ +=np.dot(bb,bb)
    s =  sqrt(num/den)
    H = np.dot(AA.T,BB)
    U, S, Vt = np.linalg.svd(H)
    R = np.dot(Vt.T,U.T)

    if np.linalg.det(R) < 0:
        Vt[2,:] *= -1
        R = Vt.T * U.T

    # TRANSLATION ESTIMATION
    t = -s*np.dot(R,centroid_A.T) + centroid_B.T

    assert(isRotationMatrix(R))
    return R, t, s



def likelihood_models(y, x, s, R, sigma, weights):
    """ 
        Log likelihood function of the alignment models
    """
    a = 0
    if np.linalg.det(sigma) <= 0:
        print("Erreur")
        return None
    inv_sigma = np.linalg.inv(sigma)
    for i in range(y.shape[1]):
        y_i = y[:,i]
        x_i = x[:,i]
        a += 0.5*weights[i]*((y_i - s*R.dot(x_i)).T.dot(inv_sigma).dot((y_i - s*R.dot(x_i))) + math.log(np.linalg.det(sigma)))
    return a



def standard_reg(Y, X, R0, s0, t0, maxiter):
    """
        Alignment with non-closed form solution with non isotropic variance.
    """
    X_ = X- np.mean(X, axis = 1, keepdims = True)
    Y_ = Y- np.mean(Y, axis = 1, keepdims = True)
    D, N = X.shape
    s = s0
    R = R0

    q = quaternion_from_matrix(R)
    errs = np.zeros((1,N))
    hist_sigma = []
    reason_crash = None

    for iter in range(maxiter):
        # update Sig:
        A = Y_ - s*R.dot(X_)
        Sig = np.true_divide(A.dot(A.T),N) + 1e-6*np.eye(D)
        hist_sigma.append(np.trace(Sig))
        
        # update scale:
        invSig = np.linalg.inv(Sig)
        if np.isnan(invSig).any():
            reason_crash = "invSig"
            return None, None, None, None, None, None, None, reason_crash

        alpha = np.ones((1,N))
        s = compute_scale(X_,Y_,R,invSig,alpha)
        if math.isnan(s):
            reason_crash = "s"
            return None, None, None, None, None, None, None, reason_crash
        q, A, B = min_rotation_over_q(X_,Y_,s,invSig,alpha,N,q,opti="SLSQP",reg_param=0)
        q = np.true_divide(q,np.linalg.norm(q)) 
        R = rot(q)
        ll = likelihood_models(Y_, X_, s, R, Sig, np.ones(68))
        if iter > 0 and math.fabs(ll_prev - ll)/math.fabs(ll_prev) < 0.01:
            break
        ll_prev = ll
    if iter == maxiter-1:
        rien
    t = np.mean(Y, axis = 1, keepdims = True) - s*R.dot(np.mean(X, axis = 1, keepdims = True))
    for i in range(N):  
        x_i = X[:,i].reshape(D,1)
        y_i = Y[:,i].reshape(D,1)
        r_i = y_i-s*R.dot(x_i)-t
        errs[0,i] = (r_i.T).dot(invSig).dot(r_i)+1e-8
    return R, s, Sig, t, errs, hist_sigma, iter, reason_crash



def compare(alpha, inliers):
    if inliers is None:
        return -1
    alpha = np.reshape(alpha,(68))
    inliers = np.reshape(inliers,(68))
    res = 0
    for i in range(len(alpha)):
        res += abs(alpha[i]-inliers[i])/68
    return res


def robustGumRegSimplified(Y, X, R0, s0, t0, maxiter, inliers, parallel=False, amplitudeUniform=100, pi=0.5):
    """
        Alignment using the GUM model.
        Gamma is not updated.
    """
    hist_sigma = []
    hist_pi = []
    hist_alpha = []

    Xc = X.copy()
    Yc = Y.copy()
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    D, N = X.shape
    s = s0
    R = R0 
    q = quaternion_from_matrix(R)
    
    A = Yc - s*R.dot(Xc)
    Sig_in0 = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)
    Sig_in = Sig_in0.copy()
    
    r0 = Yc-s*R.dot(Xc)
    gamma = 1./(amplitudeUniform**3) 
    ratioPi = (1.-pi)/pi
    reason_crash = None
    
    for iter in range(maxiter):
        # E-step:
        beta = EStepGumSimplified(Yc-s*R.dot(Xc), ratioPi, Sig_in, gamma)
        if np.isnan(beta).any():
            reason_crash = "alphas"
            return None, None, None, None, None, None, None, None, None, reason_crash, None 
        beta = np.reshape(beta,(1,-1))
        alpha = beta

        prec_out = compare(alpha, inliers)
        hist_alpha.append(np.mean(alpha)) 
        Xc, Yc, Xb, Yb = center_datas(X,Y,alpha,N)
        

        pi = (1./N)*sum(beta[0]) 
        if math.isnan(pi):
            reason_crash = "pi"
            return None, None, None, None, None, None, None, None, None, reason_crash, None 
        hist_pi.append(pi)
        ratioPi	= (1.-pi)/(pi)
        assert(isinstance(ratioPi, float))
            
        A = Yc - s*R.dot(Xc)
        A_ = np.copy(A)
        
        sum_resp = np.sum(alpha[0])
        for i in range(N):
            A[:,i] *= A_[:,i].T*alpha[0,i]/sum_resp
            
        Sig_in += 1e-6*np.eye(D)
        hist_sigma.append(np.trace(Sig_in))
        invSig = np.linalg.inv(Sig_in)
        if np.isnan(invSig).any():
            reason_crash = "invSig"
            return None, None, None, None, None, None, None, None, None, reason_crash, None 
        s = compute_scale(Xc,Yc,R,invSig,alpha)
        if s is None:
            reason_crash = "s"
            return None, None, None, None, None, None, None, None, None, reason_crash, None 
        q, A, B = min_rotation_over_q(Xc,Yc,s,invSig,alpha,N,q,opti="SLSQP_JAC",reg_param=0)
        q = q/np.linalg.norm(q) #TODO check if q is unit
        R = rot(q)

        ll = likelihood_models(Yc, Xc, s, R, Sig_in, alpha[0,:])
        if iter > 0 and math.fabs(ll_prev - ll)/math.fabs(ll_prev) < 0.01:
            break
        ll_prev = ll


    t = Yb - s*R.dot(Xb)
    t = np.reshape(t,(3,1))
    return R, s, Sig_in, t, hist_alpha, hist_pi, hist_sigma, prec_out, iter, reason_crash, alpha 





def robust_GUM_reg(Y, X, R0, s0, t0, maxiter, inliers):
    # Mostafa's version
    # X, Y: D*N
    hist_sigma = []
    hist_pi = []
    hist_alpha = []


    Xc = X.copy()
    Yc = Y.copy()
    
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    
    D, N = X.shape
    s = s0
    R = R0 
    
    q = quaternion_from_matrix(R)

    pi = 0.5
    
    A = Yc - s*R.dot(Xc)
    
    Sig_in0 = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)
    Sig_in = Sig_in0.copy()
    
    r0 = Yc-s*R.dot(Xc)
    gamma0 = np.max(r0)-np.min(r0)
    gammvect = np.zeros((1,maxiter))
    gamma = gamma0.copy()
    reason_crash = None 
    for iter in range(maxiter):

        # E-step:
        if np.sum(np.floor(np.isnan(Sig_in))):
            Sig_in = Sig_in0.copy()
        
        if gamma is None:
            reason_crash = "gamma"
            return None, None, None, None, None, None, None, None, iter , reason_crash
        beta = E_step_GUM(Yc-s*R.dot(Xc), pi, Sig_in, gamma)
        if np.isnan(beta).any():
            reason_crash = "alphas"
            return None, None, None, None, None, None, None, None, iter , reason_crash

        beta  = np.reshape(beta,(1,-1))
        alpha = beta
        prec_out = compare(alpha, inliers)
        hist_alpha.append(np.mean(alpha)) 
 
        Xc, Yc, Xb, Yb = center_datas(X,Y,alpha,N)
        
        pi      = (1./N)*sum(beta[0])
        hist_pi.append(pi)
            
            
        # update Sig-in:
        A = Yc - s*R.dot(Xc)
        for i in range(N):
            A[:,i] *= alpha[0,i]
            
        Sig_in = A.dot(A.T) + 1e-6*np.eye(D)
        Sig_in /= np.sum(alpha[0])
        hist_sigma.append(np.trace(Sig_in))
        
        # update scale:
        invSig = np.linalg.inv(Sig_in)
        if np.isnan(invSig).any():
            reason_crash = "invSig"
            return None, None, None, None, None, None, None, None, iter , reason_crash

   
        gamma = compute_gamma(Xc,Yc,s,R,alpha,pi,N)
        #if np.isnan(gamma):
        #    gamma = gamma0.copy()
            
        s = compute_scale(Xc,Yc,R,invSig,alpha)
        
        if s is None:
            reason_crash = "s"
            return None, None, None, None, None, None, None, None, iter, reason_crash 

        q,A,B           = min_rotation_over_q(Xc,Yc,s,invSig,alpha,N,q,opti="SLSQP_JAC",reg_param=0)
    #    q = opt_q_over_manifold(X,Y,s,invSig,alpha,N,q)
    
        q               = q/np.linalg.norm(q) #TODO check if q is unit
        R               = rot(q)
        ll = likelihood_models(Yc, Xc, s, R, Sig_in, alpha[0,:])
        if iter > 0 and math.fabs(ll_prev - ll)/math.fabs(ll_prev) < 0.01:
            #print("Break at: " + str(iter))
            break
        ll_prev = ll


        gammvect[0,iter] = gamma

    t = Yb - s*R.dot(Xb)
    t = np.reshape(t,(3,1))
            
    return R, s, Sig_in, t, hist_alpha, hist_pi, hist_sigma, prec_out, iter, reason_crash








def robust_GUM_regv2(Y, X, R0, s0, t0, maxiter, inliers):
    """
        Alignment with GUM model.
        Gamma is updated.
        Sigma is not used in the EM algorithm.
    """
    hist_sigma = []
    hist_pi = []
    hist_alpha = []

    Xc = X.copy()
    Yc = Y.copy()
    
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    
    D, N = X.shape
    s = s0
    R = R0 
    
    q = quaternion_from_matrix(R)

    pi = 0.5
    
    A = Yc - s*R.dot(Xc)
    
    Sig_in0 = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)
    Sig_in = Sig_in0.copy()
    
    r0 = Yc-s*R.dot(Xc)
    gamma0 = np.max(r0)-np.min(r0)
    gammvect = np.zeros((1,maxiter))
    gamma = gamma0.copy()
    
    for iter in range(maxiter):

        # E-step:
        if np.sum(np.floor(np.isnan(Sig_in))):
            Sig_in = Sig_in0.copy()
        try: 
            beta = E_step_GUM(Yc-s*R.dot(Xc), pi, Sig_in, gamma)
        except Exception as e:
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, None 
        beta  = np.reshape(beta,(1,-1))
        alpha = beta
        prec_out = compare(alpha, inliers)
        hist_alpha.append(np.mean(alpha))

        if np.allclose(alpha,np.zeros(alpha.shape)):
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, None
        Xc, Yc, Xb, Yb = center_datas(X,Y,alpha,N)
        pi = (1./N)*sum(beta[0])
        hist_pi.append(pi)
            
        # update Sig-in:
        A = Yc - s*R.dot(Xc)
        for i in range(N):
            A[:,i] *= alpha[0,i]
            
        Sig_in = A.dot(A.T) + 1e-6*np.eye(D)
        Sig_in /= np.sum(alpha[0])
        hist_sigma.append(np.trace(Sig_in))

        # update scale:
        invSig = np.linalg.inv(Sig_in)
    
        gamma = compute_gamma(Xc,Yc,s,R,alpha,pi,N)
        if gamma is None or np.isnan(gamma):
            gamma = gamma0.copy()
            
        res = alignQuatCF(Yc, Xc, alpha) 
        if res is None:
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, prec_out
        R, _, s = res
        if R is None:
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, prec_out
        
        gammvect[0,iter] = gamma
        ll = likelihood_models(Yc, Xc, s, R, Sig_in, alpha[0,:])
        if ll != None and (iter > 0 and math.fabs(ll-ll_prev) < 0.01):
            #print("Break at: " + str(iter))
            break
        ll_prev = ll


    t = Yb - s*R.dot(Xb)
    t = np.reshape(t,(3,1))
            
    return R, s, Sig_in, t, alpha, gammvect, hist_alpha, hist_pi, hist_sigma, prec_out



def robust_GUM_regv3(Y, X, R0, s0, t0, maxiter, inliers):
    """
        Alignment with GUM model.
        Gamma is updated.
        Sigma is used in the EM algorithm.
    """

    hist_sigma = []
    hist_pi = []
    hist_alpha = []


    Xc = X.copy()
    Yc = Y.copy()
    
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    
    D, N = X.shape
    s = s0
    R = R0 
    
    q = quaternion_from_matrix(R)

    pi = 0.5
    
    A = Yc - s*R.dot(Xc)
    
    Sig_in0 = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)
    Sig_in = Sig_in0.copy()
    
    r0 = Yc-s*R.dot(Xc)
    gamma0 = np.max(r0)-np.min(r0)
    gammvect = np.zeros((1,maxiter))
    gamma = gamma0.copy()
    
    for iter in range(maxiter):

        # E-step:
        if np.sum(np.floor(np.isnan(Sig_in))):
            Sig_in = Sig_in0.copy()
            
        beta = E_step_GUM(Yc-s*R.dot(Xc), pi, Sig_in, gamma)
        beta  = np.reshape(beta,(1,-1))
        alpha = beta
        prec_out = compare(alpha, inliers)
        hist_alpha.append(np.mean(alpha))

        if np.allclose(alpha,np.zeros(alpha.shape)):
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, prec_out

        
        Xc, Yc, Xb, Yb = center_datas(X,Y,alpha,N)
        pi = (1./N)*sum(beta[0])
        hist_pi.append(pi)

        # update Sig-in:
        A = Yc - s*R.dot(Xc)
        for i in range(N):
            A[:,i] *= alpha[0,i]
            
        Sig_in = A.dot(A.T) + 1e-6*np.eye(D)
        Sig_in /= np.sum(alpha[0])
        hist_sigma.append(np.trace(Sig_in))

        # update scale:
        invSig = np.linalg.inv(Sig_in)
    
        gamma = compute_gamma(Xc,Yc,s,R,alpha,pi,N)
        if gamma is None or np.isnan(gamma):
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, prec_out

            
        s = compute_scale(Xc,Yc,R,invSig,alpha)
        if s is None:
            return None, None, None, None, None, None, hist_alpha, hist_pi, hist_sigma, prec_out


        q, A, B = min_rotation_over_q(Xc,Yc,s,invSig,alpha,N,q,opti="SLSQP_JAC",reg_param=0)
        q = q/np.linalg.norm(q) 
        R = rot(q)
        
        gammvect[0, iter] = gamma
        ll = likelihood_models(Yc, Xc, s, R, Sig_in, alpha[0,:])
        if iter > 0 and math.fabs(ll-ll_prev) < 0.01:
            #print("Break at: " + str(iter))
            break
        ll_prev = ll

    t = Yb - s*R.dot(Xb)
    t = np.reshape(t,(3,1))
            
    return R, s, Sig_in, t, alpha, gammvect, hist_alpha, hist_pi, hist_sigma, prec_out


def robust_Student_reg(Y, X, R0, s0, t0, maxiter, inliers):
    hist_sigma = []
    hist_alpha = []
    hist_pi = []

    Xc = X.copy()
    Yc = Y.copy()
    
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    
    D, N = X.shape
    s = s0
    R = R0 
    
    q = quaternion_from_matrix(R)
    
    A = Yc - s*R.dot(Xc)
    
    Sig_in = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)
        
    invSig = np.linalg.inv(Sig_in)
        
    nu=1*np.ones((1,N))
    mu=1*np.ones((1,N))
    reason_crash = None

    for iter in range(maxiter):

        # E-step:
        a, b, w = E_step_Student(Yc-s*R.dot(Xc), mu, nu, invSig, N)
        if np.isnan(w).any():
            reason_crash = "w"
            return None, None, None, None, None, None, None, None, reason_crash
        w  = np.reshape(w,(1,-1))

        prec_out = compare(w/max(w), inliers)
        assert(w.shape==(1,N))    
        
        Xc, Yc, Xb, Yb = center_datas(X,Y,w,N)
                    
        # update Sig-in:
        A = Yc - s*R.dot(Xc)
        for i in range(N):
            A[:,i] *= float(w[0,i])
            
        Sig_in = A.dot(A.T) + 1e-6*np.eye(D)
        Sig_in /= N
        hist_sigma.append(np.trace(Sig_in))

        # update scale:
        invSig = np.linalg.inv(Sig_in)
        if np.isnan(invSig).any():
            reason_crash = "invSig"
            return None, None, None, None, None, None, None, None, reason_crash

        value = invpsi(psi(a[0,0])-1./N*sum([log(b[0,j]) for j in range(N)]))
        if math.isnan(value):
            reason_crash = "invPsi"
            return None, None, None, None, None, None, None, None, reason_crash


        for i in range(N):
            mu[0,i] = value
        hist_pi.append(mu[0,0])

        s = compute_scale(Xc,Yc,R,invSig,w)
        if math.isnan(s):
            reason_crash = "s"
            return None, None, None, None, None, None, None, None, reason_crash

       
    
        q,A,B = min_rotation_over_q(Xc,Yc,s,invSig,w,N,q,opti="SLSQP_JAC",reg_param=0)
    
        q = q/np.linalg.norm(q) #TODO check if q is unit
        R = rot(q)
    t = Yb - s*R.dot(Xb)
    t = np.reshape(t,(3,1))
            
    return R, s, Sig_in, t, w, hist_pi, hist_sigma, prec_out, reason_crash


"""
    SUBFUNCTIONS FOR ALIGNMENT.
"""

def compute_scale(x, y, R, sigmainv, alpha):
    """
        Compute the scale in a transformation.
    """
    assert(x.shape==(3,len(x[0])))
    assert(y.shape==(3,len(x[0])))
    assert(R.shape==(3,3))
    assert(sigmainv.shape==(3,3))
    a = np.sqrt(np.sum(alpha*np.einsum('nj,jk,nk->n', y.T, sigmainv, y.T)))
    a = sum([alpha[0,i]*distance.mahalanobis(y.T[i],0*y.T[i].T, sigmainv)**2 for i in range(len(y.T))])
    a = sqrt(a)
    b = sum([alpha[0,i]*distance.mahalanobis(fast_dot(R,x.T[i]),0*fast_dot(R,x.T[i]), sigmainv)**2 for i in range(len(y.T))])
    if np.allclose(alpha,np.zeros(alpha.shape)):
        #alpha = np.ones(alpha.shape)
        return None #compute_scale(x, y, R, np.eye(3), alpha) 
    if b == 0:
        return None #compute_scale(x, y, R, np.eye(3), alpha) 
    b = sqrt(b)
    s_star = a/b
    return s_star



def T(q, s, A, B, sigmainv):
    """
        Compute T(q), cf paper.
    """
    R = rot(q)
    T = fast_dot(sigmainv,(s**2*fast_dot(fast_dot(R,A),R.T)-2*s*fast_dot(R,B)))
    t = np.trace(T)
    return t



def  Tder(q, s, A, B, sigmainv):
    """
        Compute T'(q), cf paper.
    """
    d0 = np.array([[q[0],q[3],-q[2]],
                    [-q[3],q[0],q[1]],
                    [q[2],-q[1],q[0]]])

    d1 = np.array([[q[1],q[2],q[3]],
                    [q[2],-q[1],q[0]],
                    [q[3],-q[0],-q[1]]])

    d2 = np.array([[-q[2],q[1],-q[0]],
                    [q[1],q[2],q[3]],
                    [q[0],q[3],-q[2]]])

    d3 = np.array([[-q[3],q[0],q[1]],
                    [-q[0],-q[3],q[2]],
                    [q[1],q[2],q[3]]])

    R = rot(q)

    M = 2*fast_dot(sigmainv,(s**2*fast_dot(R,A.T)-s*B.T))
    a = 2*np.trace(fast_dot(M,d0))
    b = 2*np.trace(fast_dot(M,d1))
    c = 2*np.trace(fast_dot(M,d2))
    d = 2*np.trace(fast_dot(M,d3))
    m = [a,b,c,d]
    return  m



def center_datas(x,y,alpha,N):
    assert(x.shape==(3,N))
    assert(y.shape==(3,N))
    x_b = sum([alpha[0,i]*x[:,i] for i in range(N)])/sum(alpha[0,:]) 
    y_b = sum([alpha[0,i]*y[:,i] for i in range(N)])/sum(alpha[0,:])
    x_b = np.reshape(x_b,(3,1))
    y_b = np.reshape(y_b,(3,1))
    x_c = x - np.tile(x_b,(1,N))
    y_c = y - np.tile(y_b,(1,N))
    assert(x_c.shape==(3,N) and y_c.shape==(3,N) and x_b.shape==(3,1) and y_b.shape==(3,1))
    return x_c,y_c,x_b,y_b


    
def E_step(r_in, r_out, pi, Sig_in, Sig_out):
    beta = []
    for i in range(len(r_in.T)):
        in_part     = scipy.stats.multivariate_normal.pdf(r_in.T[i],[0,0,0],Sig_in)
        out_part    = scipy.stats.multivariate_normal.pdf(r_out.T[i],[0,0,0],Sig_out)
        
        beta_       = np.true_divide(pi*in_part,((pi*in_part+(1.-pi)*out_part)))
        beta.append(beta_)
    return beta



def func(x,pi,sigma,gamma):
    no      = scipy.stats.multivariate_normal.pdf(x,[0,0,0],sigma)
    beta_   = pi*no/((pi*no+(1.-pi)*gamma))
    return beta_



def E_step_GUM(r,pi,sigma,gamma,parallel=False):
   beta = []
   for i in range(len(r.T)):
       no       = scipy.stats.multivariate_normal.pdf(r.T[i],[0,0,0],sigma)
       beta_    = pi*no/((pi*no+(1.-pi)*gamma))
       beta.append(beta_)
   return beta



def E_step_Student(r,mu,nu,sigma_inv,N):
    a = np.zeros((1,N))
    b = np.zeros((1,N))
    w = np.zeros((1,N))
    for i in range(N):
        a[0,i] = mu[0,i] + 3./2.
        b[0,i] = nu[0,i] + np.dot(r[:,i],np.dot(sigma_inv,r[:,i]).T)*0.5
        w[0,i] = a[0,i]/b[0,i]
    return a,b,w



def compute_gamma(x_c,y_c,s,R,alpha,pi,N):
    gamma   = 1
    r       = y_c-s*R.dot(x_c)
    for k in range(3):
        c1  = 1./(N*(1.-pi))*sum([(1-alpha[0,i])*r[k,i] for i in range(N)])
        c2  = 1./(N*(1.-pi))*sum([(1-alpha[0,i])*r[k,i]**2 for i in range(N)])
        tmp = (c2-c1**2)
        tmp = 1./(N*(1.-pi))*sum([(1-alpha[0,i])*(c1-r[k,i])**2 for i in range(N)])
        try:
            assert(tmp>=0)
        except Exception as e:
            print(e)
            return None
        gamma *= 2*sqrt(3*tmp)
    return 1./gamma





def baseline_reg(Y, X, R0, s0, t0):
    X_ =X- np.mean(X, axis = 1, keepdims = True)
    Y_ =Y- np.mean(Y, axis = 1, keepdims = True)
    D, N = X.shape
    s = s0
    R = R0

    q = quaternion_from_matrix(R)
    errs = np.zeros((1,N))
    
    # update Sig:
    A = Y_ - s*R.dot(X_)

    s = compute_scale(X_,Y_,R,np.eye(3),np.ones((1,N)))

    q,A,B           = min_rotation_over_q(X_,Y_,s,np.eye(3),np.ones((1,N)),N,q,opti="SLSQP_JAC",reg_param=0)
    #q               = opt_q_over_manifold(X,Y,s,invSig,alpha,N,q)

    q               = np.true_divide(q,np.linalg.norm(q)) #TODO check if q is unit
    R               = rot(q)

    t = np.mean(Y, axis = 1, keepdims = True) - s*R.dot(np.mean(X, axis = 1, keepdims = True))
    for i in range(N):  
        x_i = X[:,i].reshape(D,1)
        y_i = Y[:,i].reshape(D,1)
        r_i = y_i-s*R.dot(x_i)-t
        errs[0,i] = (r_i.T).dot(np.eye(3)).dot(r_i)+1e-8
    return R, s, np.eye(3), t, errs
 




def EStepGumSimplified(r,ratioPi,sigma,gamma):
    beta = []
    for i in range(len(r.T)):
        no  = scipy.stats.multivariate_normal.pdf(r.T[i],[0,0,0],sigma)
        beta_ = no/((no+ratioPi*gamma))
        beta.append(beta_)
    return beta


def robust_reg(Y, X, R0, s0, t0, maxiter):

    # X, Y: D*N
    X -= np.mean(X, axis = 1, keepdims = True)
    Y -= np.mean(Y, axis = 1, keepdims = True)
    D, N = X.shape
    s = s0
    R = R0
    t = 0*t0

    errs = np.zeros(maxiter)
    q = quaternion_from_matrix(R)
    O = Y - s*R.dot(X)- t.dot(np.ones((1,N)))
    lams = 1./(np.linalg.norm(O, axis=0)+1e-10)
    lams = np.reshape(lams, (1,N))

    for iter in range(maxiter):

        # update Sig:
        A = Y - s*R.dot(X)-O - t.dot(np.ones((1,N)))
        Sig = A.dot(A.T)/N + 1e-6*np.eye(D)

        # update scale:
        invSig = np.linalg.inv(Sig)

        alpha = np.ones((1,N))
        s = compute_scale(X,Y-O- t.dot(np.ones((1,N))),R,invSig,alpha)

        # update the rotation:
        q,A,B           = min_rotation_over_q(X,Y-O,s,invSig,alpha,N,q,opti="SLSQP_JAC",reg_param=0)

        q               = q/np.linalg.norm(q) #TODO check if q is unit
        R               = rot(q)

        # update outliers:
        for i in range(1):
            Yhh = Y + np.mean(O, axis = 1).reshape((D,1))
            O = outlier_update(Yhh - s*R.dot(X) , O, invSig, lams)
        lams = 1./(np.linalg.norm(O, axis=0)+1e-8)
        lams = np.reshape(lams, (1,N))


        A = Y - s*R.dot(X)-O
        errs[iter] = np.matrix.trace(A.T.dot(invSig).dot(A))+np.log(np.linalg.det(Sig))

    t = np.mean(Y-O, axis = 1).reshape((D,1)) - s*R.dot(np.mean(X, axis = 1).reshape((D,1)))
    return R, s, Sig, t, lams




def expected_ll(s,R,t,y,x,mu,sigma,gamma,D,N):
    ll  = 0
    sq_ = 0
    b_  = 0
    c_  = 0
    invSig  = np.linalg.inv(sigma)
    
    for i in range(N):
        sigma_i = np.linalg.inv(invSig+(1./gamma[0,i])*np.eye(D))
        x_i     = x[:,i].reshape(D,1)
        y_i     = y[:,i].reshape(D,1)
        tmp     = y_i-s*R.dot(x_i)-mu[:,i].reshape(D,1)
        sq      = (tmp.T).dot(invSig).dot(tmp)
        b       = (1./gamma[0,i])*( np.trace(sigma_i) + np.linalg.norm(mu[:,i].reshape(D,1)-t)**2 )

        c       =     D*log(gamma[0,i])   +   log(np.linalg.det(sigma))       +   np.trace(invSig.dot(sigma_i))

        sq_ += sq[0,0]
        b_ += b
        c_ += c
        ll += sq[0,0]+b+c 
    return ll,sq_,b_,c_



def robust_reg_EMtest(Y, X, R0, s0, t0, maxiter):
    # X, Y: D*N
    D, N = X.shape
    Xc = X.copy()
    Yc = Y.copy()
    
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    
    s = s0
    R = R0
    t = t0
    
    A = Yc - s*R.dot(Xc)
    
    Sig = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)

    q = quaternion_from_matrix(R)
    O = Y - s*R.dot(X)
    
    ti = t.dot(np.ones((1,N)))
    assert(ti.shape == (D,N))
    lams = np.true_divide((np.linalg.norm(O-ti, axis=0))**2+np.trace(Sig),D)
    lams = np.reshape(lams, (1,N))
    lams = 1e5*np.ones((1,N))
    
    invSig = np.linalg.inv(Sig)
    stop_criterium_1 = []
    stop_criterium_2 = []
    stop_criterium_3 = []
    for iter in range(maxiter):
        s_prev = s
        R_prev = R
        t_prev = t 
        # E-step:
        Sig_O, mu_O, tr_sig, t_t = outlier_update_EM(Y - s*R.dot(X), t, invSig, lams)

        tdiff_array = mu_O-np.tile(t,(1,N))
        lams_prev = lams
        lams = np.true_divide((np.linalg.norm(tdiff_array,axis=0)**2 + tr_sig),D)
       
        
        alpha = np.ones((1,N))
        s = compute_scale(X,Y-mu_O,R,invSig,alpha)
    
        # update the rotation:
        q,A,B           = min_rotation_over_q(X,Y-mu_O,s,invSig,alpha,N,q,opti="SLSQP_JAC",reg_param=0)
#        q = opt_q_over_manifold(X,Y,s,invSig,alpha,N,q)
    
        q               = np.true_divide(q, np.linalg.norm(q)) #TODO check if q is unit
        R               = rot(q)
    
    
        # update the shift:
        a = np.zeros((D,1))
        b = 0
    
        for i in range(N):
            a += mu_O[:,[i]]/lams[0,i]
            b += 1./lams[0,i]
    
        t = a/b

        # update Sig:
        A = Y - s*R.dot(X)-mu_O
        Sig = np.true_divide(A.dot(A.T) + Sig_O, N) + 1e-6*np.eye(D)
    
        # update scale:
        invSig = np.linalg.inv(Sig)

    return R, s, Sig, t, lams






def expected_complete_loglikelihood(Y,X,s,R,t,N,D,Sig,invSig,Omegas,a,bs,mus,invLambda,Lambda,alpha,beta):
    tmp1 = 0
    tmp2 = 0
    tmp3 = 0
    tmp4 = 0
    tmp5 = 0
    tmp6 = 0
    diga = scipy.special.digamma(a)
    for i in range(N):
        x_i         = X[:,i].reshape((3,1))
        y_i         = Y[:,i].reshape((3,1))
        Omegas_i    = Omegas[:,:,i]
        mus_i       = mus[:,:,i].reshape((3,1))
        tmp1        += (y_i-s*R.dot(x_i)-mus_i).T.dot(invSig).dot((y_i-s*R.dot(x_i)-mus_i))[0,0]
        tmp2        += np.trace(invSig.dot(Omegas_i))
        tmp3        += diga-log(bs[0,i])
        tmp4        += np.trace((Omegas_i+mus_i.dot(mus_i.T)-2*mus_i.dot(t.T)+t.dot(t.T)).dot(invLambda)*a/bs[0,i])
        tmp5        += diga-log(bs[0,i])
        tmp6        += 1./bs[0,i]
    res = -D*N/2.*log(2*pi)-N/2.*log(np.linalg.det(Sig)) -0.5*tmp1 - 0.5*tmp2 \
    -D*N/2.*log(2*pi)+D/2.*tmp3 - 0.5*tmp4 \
    -N/2.*log(np.linalg.det(Lambda))+N*(alpha*log(beta)-scipy.special.gammaln(alpha)) \
    +(alpha-1)*tmp5 -beta*a*tmp6
    return res




"""
    GEOMETRICAL OPERATIONS
"""

def proj_rot(R0):
    d           = R0.shape[0]
    U, S, V     = np.linalg.svd(R0)
    C           = np.eye(d)
    C[-1,-1]    = np.linalg.det(U.dot(V.T))
    R		= U.dot(C).dot(V.T)
    return R



def proj_orth(R0):
    U, S, V	 = np.linalg.svd(R0)
    R		   = U.dot(V.T)
    return R



def projSO3(R0):
	M	 = R0.T.dot(R0)
	D, U	 = np.linalg.eig(M)
	s	 = np.sign(np.linalg.det(R0))
	d	 = 1./np.sqrt(D)
	d[-1]	*= s
	R	 = R0.dot(U).dot(np.diag(d)).dot(U.T)
	return R



def random_rot(D):
    R0 = np.random.randn(D, D) 
    R = proj_rot(R0)
    return R




def rot_from_angles(gamma, phi, psi):
    A = np.array(   [[math.cos(gamma), -math.sin(gamma), 0],
                     [math.sin(gamma), math.cos(gamma), 0],
                     [0, 0, 1]
                    ])

    B = np.array(   [[math.cos(phi), 0, math.sin(phi)],
                     [0, 1, 0],
                     [-math.sin(phi), 0, math.cos(phi)]
                    ])

    C = np.array(   [[1, 0, 0],
                     [0, math.cos(psi), -math.sin(psi)],
                     [0, math.sin(psi), math.cos(psi)]
                    ])
    
    return A.dot(B).dot(C)




def rot(q):
    """ Convert quaternion to rotation matrix"""
    M = np.array([
                    [q[0]**2+q[1]**2-q[2]**2-q[3]**2, 2*(q[1]*q[2]-q[0]*q[3]),2*(q[1]*q[3]+q[0]*q[2])],
                    [2*(q[1]*q[2]+q[0]*q[3]),q[0]**2-q[1]**2+q[2]**2-q[3]**2,2*(q[2]*q[3]-q[0]*q[1])],
                    [2*(q[1]*q[3]-q[0]*q[2]),2*(q[2]*q[3]+q[0]*q[1]),q[0]**2-q[1]**2-q[2]**2+q[3]**2]
                ])
    return M



def quaternion_from_matrix(matrix, isprecise=False):
    """Return quaternion from rotation matrix.

    If isprecise is True, the input matrix is assumed to be a precise rotation
    matrix and a faster algorithm is used.

    >>> q = quaternion_from_matrix(numpy.identity(4), True)
    >>> numpy.allclose(q, [1, 0, 0, 0])
    True
    >>> q = quaternion_from_matrix(numpy.diag([1, -1, -1, 1]))
    >>> numpy.allclose(q, [0, 1, 0, 0]) or numpy.allclose(q, [0, -1, 0, 0])
    True
    >>> R = rotation_matrix(0.123, (1, 2, 3))
    >>> q = quaternion_from_matrix(R, True)
    >>> numpy.allclose(q, [0.9981095, 0.0164262, 0.0328524, 0.0492786])
    True
    >>> R = [[-0.545, 0.797, 0.260, 0], [0.733, 0.603, -0.313, 0],
    ...      [-0.407, 0.021, -0.913, 0], [0, 0, 0, 1]]
    >>> q = quaternion_from_matrix(R)
    >>> numpy.allclose(q, [0.19069, 0.43736, 0.87485, -0.083611])
    True
    >>> R = [[0.395, 0.362, 0.843, 0], [-0.626, 0.796, -0.056, 0],
    ...      [-0.677, -0.498, 0.529, 0], [0, 0, 0, 1]]
    >>> q = quaternion_from_matrix(R)
    >>> numpy.allclose(q, [0.82336615, -0.13610694, 0.46344705, -0.29792603])
    True
    >>> R = random_rotation_matrix()
    >>> q = quaternion_from_matrix(R)
    >>> is_same_transform(R, quaternion_matrix(q))
    True
    >>> is_same_quaternion(quaternion_from_matrix(R, isprecise=False),
    ...                    quaternion_from_matrix(R, isprecise=True))
    True
    >>> R = euler_matrix(0.0, 0.0, numpy.pi/2.0)
    >>> is_same_quaternion(quaternion_from_matrix(R, isprecise=False),
    ...                    quaternion_from_matrix(R, isprecise=True))
    True

    """
    assert(isRotationMatrix(matrix))
    M = np.array(matrix, dtype=np.float64, copy=False)[:4, :4]
    if isprecise:
        q = np.empty((4, ))
        t = np.trace(M)
        if t > M[3, 3]:
            q[0] = t
            q[3] = M[1, 0] - M[0, 1]
            q[2] = M[0, 2] - M[2, 0]
            q[1] = M[2, 1] - M[1, 2]
        else:
            i, j, k = 0, 1, 2
            if M[1, 1] > M[0, 0]:
                i, j, k = 1, 2, 0
            if M[2, 2] > M[i, i]:
                i, j, k = 2, 0, 1
            t = M[i, i] - (M[j, j] + M[k, k]) + M[3, 3]
            q[i] = t
            q[j] = M[i, j] + M[j, i]
            q[k] = M[k, i] + M[i, k]
            q[3] = M[k, j] - M[j, k]
            q = q[[3, 0, 1, 2]]
        q *= 0.5 / sqrt(t * M[3, 3])
    else:
        m00 = M[0, 0]
        m01 = M[0, 1]
        m02 = M[0, 2]
        m10 = M[1, 0]
        m11 = M[1, 1]
        m12 = M[1, 2]
        m20 = M[2, 0]
        m21 = M[2, 1]
        m22 = M[2, 2]
        # symmetric matrix K
        K = np.array([[m00-m11-m22, 0.0,         0.0,         0.0],
                         [m01+m10,     m11-m00-m22, 0.0,         0.0],
                         [m02+m20,     m12+m21,     m22-m00-m11, 0.0],
                         [m21-m12,     m02-m20,     m10-m01,     m00+m11+m22]])
        K /= 3.0
        # quaternion is eigenvector of K that corresponds to largest eigenvalue
        w, V = np.linalg.eigh(K)
        q = V[[3, 0, 1, 2], np.argmax(w)]
    if q[0] < 0.0:
        np.negative(q, q)
    assert(np.allclose(np.linalg.norm(q),1))
    return q/np.linalg.norm(q)



def isRotationMatrix(R):
    # square matrix test
    if R.ndim != 2 or R.shape[0] != R.shape[1]:
        return False
    should_be_identity = np.allclose(R.dot(R.T), np.identity(R.shape[0], np.float))
    should_be_one = np.allclose(np.linalg.det(R), 1)
    return should_be_identity and should_be_one



def Q(a):
    return np.asarray([[0,-a[0],-a[1],-a[2]],[a[0],0,-a[2],a[1]],[a[1],a[2],0,-a[0]],[a[2],-a[1],a[0],0]])



def W(a):
    return np.asarray([[0,-a[0],-a[1],-a[2]],[a[0],0,a[2],-a[1]],[a[1],-a[2],0,a[0]],[a[2],a[1],-a[0],0]])






"""
    METRICS
"""

def rot_metrics(R1, R2, metric = "FroDiff"):
	
	# R1 is the ground truth
	if metric == "FroDiff":
	    out =  np.linalg.norm(R1-R2,'fro')/np.linalg.norm(R1,'fro')
	elif metric == "FroEye":
	    out = np.linalg.norm(np.eye(3)-R1.dot(R2.T),'fro')
	elif metric == "QuaDiff":
	    q1 = np.asarray(quaternion_from_matrix(R1))
	    q2 = np.asarray(quaternion_from_matrix(R2))
	    out = np.min([np.linalg.norm(q1-q2),np.linalg.norm(q1+q2)])     
	elif metric == "QuaProd":
	    q1 = quaternion_from_matrix(R1)
	    q2 = quaternion_from_matrix(R2)
	    out = 1-np.abs(np.dot(q1,q2))
	elif metric == "sub_quat":    
		q1 = np.asarray(quaternion_from_matrix(R1))
		q2 = np.asarray(quaternion_from_matrix(R2))
		out = min(np.linalg.norm(q2-q1),np.linalg.norm(q1+q2))
	else:
		None 
	return out



def rotationMetricAcos(q,qref):
    """
        Compute a rotation metric with arccos.
    """
    assert(np.allclose(np.linalg.norm(q),1) and np.allclose(np.linalg.norm(qref),1))
    R       = rot(q)
    Rref    = rot(qref)
    v       = np.asarray([1,0,0])
    p       = v.T.dot(R.T.dot(Rref)).dot(v)
    p       = min([p,1])
    if p>1:
        print("ATTENTION THIS WAS NOT ESTIMATED EXACTLY AS ROTATION")
        p = p/np.linalg.norm(p)
    return max((180./math.pi)*math.acos(math.sqrt(p)),1e-16)





"""
    OPTIMIZATION
"""

def min_rotation_over_q(x_c,y_c,s_star,sigmainv,alpha,N,q_init,opti,reg_param):
        #A_       = sum([alpha[0,i]*(np.reshape(x_c[:,i],(3,1))*np.reshape(x_c[:,i],(3,1)).T) for i in range(N)])
        A       = np.dot(alpha*x_c,x_c.T)
        B       = np.dot(alpha*x_c,y_c.T)
        #B       = sum([alpha[0,i]*(np.reshape(x_c[:,i],(3,1))*np.reshape(y_c[:,i],(3,1)).T) for i in range(N)])
        assert(A.shape==(3,3) and B.shape==(3,3))
        f       = lambda q: T(q,s_star,A,B,sigmainv)     + reg_param*(1.-np.dot(q,q))**2 if reg_param!=0 else T(q,s_star,A,B,sigmainv)
        fp      = lambda q: Tder(q,s_star,A,B,sigmainv)  + reg_param*2*(1.-np.dot(q,q))*(-2*q) if reg_param!=0 else Tder(q,s_star,A,B,sigmainv)
        if opti == "SLSQP_JAC":
            cons    = ({'type': 'eq', 'fun': lambda q:  1.-np.dot(q,q)})
            res     = minimize(f, q_init, method='SLSQP',  jac=fp, constraints=cons)
        elif opti == "SLSQP":
            cons    = ({'type': 'eq', 'fun': lambda q:  1.-np.dot(q,q)})
            res     = minimize(f, q_init, method='SLSQP', options={'ftol':0.00000001,'maxiter':1000},constraints=cons)
        elif opti == "BFGS_JAC":
            res     = minimize(f, q_init, method='BFGS',jac=fp)
        elif opti == "BFGS":
            res     = minimize(f, q_init, method='BFGS')
        elif opti == "Newton-CG_JAC":
            res     = minimize(f, q_init, method='Newton-CG', jac=fp)
        elif opti == "global":
            res     = basinhopping(f, q_init)
        q = res.x

        return q,A,B



def opt_q_over_manifold(x_c,y_c,s_star,sigmainv,alpha,N,q_init):

        q_init = np.asarray(q_init)
        A       = sum([alpha[0,i]*(np.reshape(x_c[:,i],(3,1))*np.reshape(x_c[:,i],(3,1)).T) for i in range(N)])
        B       = sum([alpha[0,i]*(np.reshape(x_c[:,i],(3,1))*np.reshape(y_c[:,i],(3,1)).T) for i in range(N)])
        C = (s_star,A,B,sigmainv)

        q, g, out = opt_mani_mulit_ball_gbb(
            q_init,
            func_q,
            C,
            record=0,
            mxitr=600,
            gtol=1e-8,
            xtol=1e-8,
            ftol=1e-10,
            tau=1e-3)

        return q



"""
    UTILS
"""



def is_pos_def(x):
    return np.all(np.linalg.eigvals(x) > 0)


def _impose_f_order(X):
    """Helper Function"""
    # important to access flags instead of calling np.isfortran,
    # this catches corner cases.
    if X.flags.c_contiguous:
        return check_array(X.T, copy=False, order='F'), True
    else:
        return check_array(X, copy=False, order='F'), False



def fast_dot(A, B):
    return np.dot(A,B)
    if A.ndim ==1:
        A=np.reshape(A,(1,-1))
    if B.ndim ==1:
        B=np.reshape(B,(1,-1))

    """Compute fast dot products directly calling BLAS.
    This function calls BLAS directly while warranting Fortran contiguity.
    This helps avoiding extra copies `np.dot` would have created.
    For details see section `Linear Algebra on large Arrays`:
    http://wiki.scipy.org/PerformanceTips
    Parameters
    ----------
    A, B: instance of np.ndarray
    input matrices.
    """


    if A.dtype != B.dtype:
        raise ValueError('A and B must be of the same type.')
    if A.dtype not in (np.float32, np.float64):
        raise ValueError('Data must be single or double precision float.')

    dot = scipy.linalg.get_blas_funcs('gemm', (A, B))
    A, trans_a = _impose_f_order(A)
    B, trans_b = _impose_f_order(B)
    return dot(alpha=1.0, a=A, b=B, trans_a=trans_a, trans_b=trans_b)



def d_approx_psi(x):
    from numpy import clip, where
    
    if type(x) == np.ndarray:
        y = 0. * x
        y[where(x < 0.6)] = 1. / clip(x[where(x < 0.6)], 1e-154, 1e308) ** 2
        y[where(x >= 0.6)] = 1. / (x[where(x >= 0.6)] - 0.5)
        return y
    else:

        if x < 0.6:
            return 1 / clip(x, 1e-154, 1e308) ** 2
        else:
            return 1 / (x - 0.5)



def invpsi_(y, tol=1e-10, n_iter=10000, psi=psi):
    """
    Inverse digamma function
    """
    from numpy import exp
    from scipy.special import digamma
    EULER_MASCHERONI = 0.57721566490153286060651209008240243104215933593992

    ## initial value

    if y < -5 / 3. - EULER_MASCHERONI:
        x = -1 / (EULER_MASCHERONI + y)
    else:
        x = 0.5 + exp(y)

    ## Newton root finding

    for dummy in range(n_iter):

        z = digamma(x) - y

        if abs(z) < tol:
            break
        
        x -= z / d_approx_psi(x)
    if y-digamma(x)>=0.0000001:
        assert(False)

    return x 



def invpsi(X):
    from   scipy.special    import digamma
    # Based on Paul Flacker algorithm
    L=1.
    Y=np.exp(X)
    iter=0
    Maxiter =5000 
    while L > 10e-8 and iter <= Maxiter:
        Y = Y +L*np.sign(X-digamma(Y))
        L = L/2.
        iter+=1

    return Y



def invpsi_experimental(X):
    # This time X is a 2D array
    from   scipy.special    import digamma
    # Based on Paul Flacker algorithm
    L=np.ones(X.shape)
    Y=np.exp(X)
    iter=0
    Maxiter = 3
    while iter <= Maxiter: #L > 10e-3 and iter <= Maxiter:
        Y = Y +L*np.sign(X-digamma(Y))
        L = L/2.
        iter+=1
    return Y



def invpsi_new(X):
    from   scipy.special    import digamma, polygamma
    # Based on Paul Flacker algorithm
    M = np.floor(X >= -2.22)
    Y=M*(np.exp(X)+0.5)+(1-M)*(-1./(X-digamma(1)))
    Ydiff = 1e5
    Maxiter = 30
    iter = 1
    
    while Ydiff > 10e-3 and iter <= Maxiter:
        Ynew = Y -(digamma(Y)-X)/(polygamma(1,Y))
        Ydiff = np.abs(Y-Ynew)/Ynew
        Y = Ynew
        iter += 1
    return Y



#@nb.njit(fastmath=True)
def inversion(m):
    minv=np.empty(m.shape,dtype=m.dtype)
    for i in range(m.shape[0]):
        determinant_inv = 1./(m[i,0]*m[i,4]*m[i,8] + m[i,3]*m[i,7]*m[i,2] + m[i,6]*m[i,1]*m[i,5] - m[i,0]*m[i,5]*m[i,7] - m[i,2]*m[i,4]*m[i,6] - m[i,1]*m[i,3]*m[i,8])
        minv[i,0]=(m[i,4]*m[i,8]-m[i,5]*m[i,7])*determinant_inv
        minv[i,1]=(m[i,2]*m[i,7]-m[i,1]*m[i,8])*determinant_inv
        minv[i,2]=(m[i,1]*m[i,5]-m[i,2]*m[i,4])*determinant_inv
        minv[i,3]=(m[i,5]*m[i,6]-m[i,3]*m[i,8])*determinant_inv
        minv[i,4]=(m[i,0]*m[i,8]-m[i,2]*m[i,6])*determinant_inv
        minv[i,5]=(m[i,2]*m[i,3]-m[i,0]*m[i,5])*determinant_inv
        minv[i,6]=(m[i,3]*m[i,7]-m[i,4]*m[i,6])*determinant_inv
        minv[i,7]=(m[i,1]*m[i,6]-m[i,0]*m[i,7])*determinant_inv
        minv[i,8]=(m[i,0]*m[i,4]-m[i,1]*m[i,3])*determinant_inv

    return minv



#I was to lazy to modify the code from the link above more thoroughly
def inversion_3x3(m):
    m_TMP=m.reshape(m.shape[0],9)
    minv=inversion(m_TMP)
    return minv.reshape(minv.shape[0],3,3)




def robust_reg_varEMst(Y, X, R0, s0, t0, maxiter):

    # X, Y: D*N
    D, N = X.shape
    s = s0
    R = R0
    t = t0

    A = Y - s*R.dot(X) - t.dot(np.ones((1,N)))
    Sig = A.dot(A.T)/N + 1e-6*np.eye(D)
        
    q = quaternion_from_matrix(R)

    mu_s = s*np.ones(N)
    sig2_s = 1e-5*np.ones(N)

    mu_t =  Y-s*R.dot(X) #t.dot(np.ones((1,N)))
    Sig_t = 1e-5*np.eye(D).reshape(-1,1).dot(np.ones((1,N)))
    Sig_t_s = np.ones((D,N))

    mu_R = R.reshape(-1,1).dot(np.ones((1,N)))
    Sig_R = 1e-5*np.eye(D**2).reshape(-1,1).dot(np.ones((1,N)))

    LAM_i = np.zeros((D+1,D+1))
    mus_i = np.zeros(D+1)

    I_d = np.eye(D)
    I_d2 = np.eye(D**2)

    scale_par = 1e-10

    gammas = scale_par*np.ones((1,N))
    deltas = scale_par*np.ones((1,N)) #np.true_divide((np.linalg.norm(mu_t-t.dot(np.ones((1,N))), axis=0)**2 + 1e-8),D).reshape(1,-1) 
    etas = scale_par*np.ones((1,N))

    invSig = np.linalg.inv(Sig)
    
    for iter in range(maxiter):


                # Update the parameters:

        # update Sig:
        A = np.zeros((D,D))
        B_out = np.zeros((D,D))

        for i in range(N):
            x_i = X[:,i].reshape(D,1)
            y_i = Y[:,i].reshape(D,1)
            A += Sig_t[:,i].reshape(D,D)+2*mu_R[:,i].reshape(D,D).dot(x_i).dot(mu_s[i]*(mu_t[:,i].reshape(-1,1)-y_i).T+Sig_t_s[:,i])
#            A += Sig_t[:,i].reshape(D,D)+2*(mu_s[i]*(mu_t[:,i].reshape(-1,1)-y_i.reshape(-1,1))+Sig_t_s[:,i].reshape(-1,1)).dot((x_i.T).dot(mu_R[:,i].reshape(D,D).T))
            A_i = x_i.dot(x_i.T)
            B_i = (mu_R[:,i].reshape(-1,1).dot(mu_R[:,i].reshape(-1,1).T)+Sig_R[:,i].reshape(D**2,D**2))
            B_outi = np.zeros((D,D))            
            for i1 in range(D):
                B_in = np.zeros((D,D))
                for p in range(D):
                    B_in += A_i[i1,p]*B_i[p*D:(p+1)*D,i1*D:(i1+1)*D]
                B_outi += B_in.T
            B_out += (mu_s[i]**2+sig2_s[i])*B_outi

#        print(mu_t)
        YT = Y -mu_t
        Sig = np.true_divide(YT.dot(YT.T) + A.T + B_out, N) + 1e-6*np.eye(D)

        invSig = np.linalg.inv(Sig)
        
        # variational E-step:

        # Scale and translations:

        for i in range(N):
            x_i = X[:,i].reshape(D,1)
            y_i = Y[:,i].reshape(D,1)
            a_i = x_i.T.dot(R.T).dot(invSig).dot(R).dot(x_i)
            LAM_i[:D,:D] = invSig+I_d/deltas[0,i]
            LAM_i[:D,D:] = invSig.dot(R).dot(x_i)
            LAM_i[D:,:D] = LAM_i[:D,D:].T
            LAM_i[D:,D:] = 1/etas[0,i]+a_i
#            print(np.linalg.cond(LAM_i))
            mus_i[:D] = (invSig.dot(y_i)+t/deltas[0,i]).T
            mus_i[-1] = y_i.T.dot(invSig).dot(R).dot(x_i)+s/etas[0,i]

            Sig_TS = np.linalg.inv(LAM_i+1e-6*np.eye(D+1))
            mu_ts_i = Sig_TS.dot(mus_i)

            mu_s[i] = mu_ts_i[-1]
            sig2_s[i] = Sig_TS[D:,D:]

            mu_t[:,[i]] = mu_ts_i[:D].reshape(-1,1)
            Sig_t[:,[i]] = Sig_TS[:D,:D].reshape(-1,1)

            Sig_t_s[:,[i]] = Sig_TS[:D,D:]

        # update scale:

        si = 0
        b = 0

        for i in range(N):
            if mu_s[i]>0:
                si += np.true_divide(mu_s[i],etas[0,i])
                b += np.true_divide(1,etas[0,i])

        s = np.true_divide(si, b)

        # update the rotation:

        Ri = np.zeros((D,D))
        b = 0

        for i in range(N):
#            print(np.amax(mu_R[:,[i]]))
                Ri += np.true_divide(mu_R[:,[i]].reshape(D,D),gammas[0,i])
                b += np.true_divide(1,gammas[0,i])
            
#        print(Sig_R)
#        print(gammas)
        
        R = proj_rot(np.true_divide(Ri, b))
        
        # update the translation:

        ti = np.zeros((D,1))
        b = 0

        for i in range(N):
            ti += np.true_divide(mu_t[:,[i]],deltas[0,i])
            b += np.true_divide(1,deltas[0,i])
            
        t = np.true_divide(ti, b)
                
        # Update parameters:
        for i in range(N):
            tr_R = np.trace(Sig_R[:,i].reshape(D**2,D**2))
            tr_t = np.trace(Sig_t[:,i].reshape(D,D))
            tr_s = sig2_s[i]
            gammas[0,i] = np.true_divide((np.linalg.norm(R.reshape(D**2,1)-mu_R[:,i].reshape(D**2,1))**2 + tr_R), D**2)
            deltas[0,i] = np.true_divide((np.linalg.norm(t.reshape(-1,1)-mu_t[:,i].reshape(-1,1))**2 + tr_t), D)
            etas[0,i] = (s-mu_s[i])**2 + tr_s
            
        
#        print(mu_R.reshape(N,D**2))
        
    return R, s, Sig, t, gammas, deltas, etas

    

def robust_Student_Plain(Y, X, R0, s0, t0, maxiter):

    # X, Y: D*N
    Xc = X.copy()
    Yc = Y.copy()
    
    Xc -= np.mean(X, axis = 1, keepdims = True)
    Yc -= np.mean(Y, axis = 1, keepdims = True)
    
    D, N = X.shape
    s = s0
    R = R0 
    
    q = quaternion_from_matrix(R)
    
    A = Yc - s*R.dot(Xc)
    
    Sig_in = (1./N)*A.dot(A.T) + 1e-6*np.eye(D)
        
    invSig = np.linalg.inv(Sig_in)
        
    w=1*np.ones((1,N))

        
    for iter in range(maxiter):

        # E-step:   
        
                
        # update weights:
        for i in range(N):
            x_i = Xc[:,i].reshape(D,1)
            y_i = Yc[:,i].reshape(D,1)
            rep_er = y_i-s*R.dot(x_i)
            assert(rep_er.shape == (D,1))
            w[0,i] = np.true_divide( D,(rep_er.T.dot(invSig).dot(rep_er)+1e-8))
            
        Xc, Yc, Xb, Yb = center_datas(X,Y,w,N)
                    
        for in_iter in range(1):
            
            # update Sig-in:
            A = Yc - s*R.dot(Xc)
            for i in range(N):
                A[:,i] *= float(w[0,i])
                
            Sig_in = A.dot(A.T) + 1e-6*np.eye(D)
            Sig_in /= N
            
            # update scale:
            invSig = np.linalg.inv(Sig_in)
              
            
            s = compute_scale(Xc,Yc,R,invSig,w)
            
    
            q,A,B           = min_rotation_over_q(Xc,Yc,s,invSig,w,N,q,opti="SLSQP_JAC",reg_param=0)
    #        q = opt_q_over_manifold(X,Y,s,invSig,alpha,N,q)
    
            q               = q/np.linalg.norm(q) #TODO check if q is unit
            R               = rot(q)


            
    t = Yb - s*R.dot(Xb)
    t = np.reshape(t,(3,1))
            
    return R, s, Sig_in, t, w




def sample_from_generalized_t_distrib(shape,scale,size,sigma=np.eye(3)):
    omegas = np.random.gamma(shape,scale,size)
    samples = []
    for i in omegas:
        cov = 1/i*sigma
        sample = np.random.multivariate_normal([0,0,0],cov,1)
        samples.append(sample)
    samples = np.squeeze(samples)
    return samples.T

def sample_from_t_distrib():
    """
        Generate samples from a t-distribution
    """
    df = 3.
    r = t.rvs(df, size=1000)
    fig, ax = plt.subplots(1,1)
    ax.hist(r, density=True, histtype='stepfilled',alpha=0.2)
    ax.legend(loc='best', frameon=False)
    rv = t(df)
    x = np.linspace(t.ppf(0.01, df), t.ppf(0.99, df), 100)
    ax.plot(x, t.pdf(x, df), 'r-', lw=5, alpha=0.6, label='t pdf')
    plt.savefig("/home/sguy/Desktop/test.png")
    return r

def generate_noise_t():
    """
        Generate samples from a t-distribution
    """
    df = 3.
    r = t.rvs(df, size=(3,68))
    #rv = t(df)
    #x = np.linspace(t.ppf(0.01, df), t.ppf(0.99, df), 100)
    return r



def fit_a_t_distrib(data):
    """
        MLE approximation of a t-distribution models
    """
    f = Fitter(data, distributions=['t'])
    f.fit()
    return f.fitted_param['t']



