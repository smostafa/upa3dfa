import warnings
warnings.filterwarnings('ignore')

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
from scipy.spatial import ConvexHull, convex_hull_plot_2d
from numpy import loadtxt
import json
import numpy as np
import scipy.io as sio
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import os
from scipy.stats.distributions import chi2

import sys

from performance_model import build_pca_ellipsoid

from six.moves import cPickle as pickle #for performance

#%% Show some sample face images with landmarks:

image_list = ['image00710.mat', 'image04312.mat'] # Here, we have two sample images
methods = ['Bulat','PRNET','3DDFA','2DASL','3DDFA_V2'] # Five 3DFA methods

image_id = int(sys.argv[1]) # ID of the test image
alg_id = int(sys.argv[2]) # ID of the 3DFA method


img_name = image_list[image_id]
align_method = 'student' # 'student' or 'gum'

fa_method = methods[alg_id] 

mks = 7

input_folder = './AFLW2000'
save_folder = './results/figures/test_images'
ldks_folder = './results/'


model_path = './training_data_model/training_data_student_bulat.txt'

with open(model_path) as json_file:  
    model = json.load(json_file)
 
eigen_vectors = [item for item in model['eigen_vectors']]
centers = [item for item in model['center']]
eigen_values = [item for item in model['eigen_values']]

fig, axs = plt.subplots(1,2, figsize=(15, 6), facecolor='w', edgecolor='k')
fig.subplots_adjust(hspace = 0, wspace=0)

axs = axs.ravel()

file1 = os.path.join(ldks_folder,'ldks_Bulat',str(img_name[:-4])+'.jpg.txt')
file2 = os.path.join(ldks_folder,'ldks_PRNET',str(img_name[:-4])+'.txt')
file3 = os.path.join(ldks_folder,'ldks_3DDFA',str(img_name[:-4])+'.txt')
file4 = os.path.join(ldks_folder,'ldks_2DASL',str(img_name[:-4])+'.txt')
file5 = os.path.join(ldks_folder,'ldks_3DDFA_V2',str(img_name[:-4])+'.txt')
    
ldmrks1 = loadtxt(file1)
ldmrks2 = loadtxt(file2)
ldmrks3 = loadtxt(file3).T
ldmrks4 = loadtxt(file4).T
ldmrks5 = loadtxt(file5)
        
ldmrks1_a = loadtxt(os.path.join(ldks_folder,'aligned_ldks_Bulat',str(img_name[:-4])+'_'+str(align_method)+'.txt'))
ldmrks2_a = loadtxt(os.path.join(ldks_folder,'aligned_ldks_PRNET',str(img_name[:-4])+'_'+str(align_method)+'.txt'))
ldmrks3_a = loadtxt(os.path.join(ldks_folder,'aligned_ldks_3DDFA',str(img_name[:-4])+'_'+str(align_method)+'.txt'))
ldmrks4_a = loadtxt(os.path.join(ldks_folder,'aligned_ldks_2DASL',str(img_name[:-4])+'_'+str(align_method)+'.txt'))
ldmrks5_a = loadtxt(os.path.join(ldks_folder,'aligned_ldks_3DDFA_V2',str(img_name[:-4])+'_'+str(align_method)+'.txt'))

if fa_method == 'Bulat':
    ldmrks = ldmrks1
    ldmrks_a = ldmrks1_a
elif fa_method == 'PRNET':
    ldmrks = ldmrks2
    ldmrks_a = ldmrks2_a
elif fa_method == '3DDFA':
    ldmrks = ldmrks3
    ldmrks_a = ldmrks3_a 
elif fa_method == '2DASL':
    ldmrks = ldmrks4
    ldmrks_a = ldmrks4_a    
elif fa_method == '3DDFA_V2':
    ldmrks = ldmrks5
    ldmrks_a = ldmrks5_a
    
img = mpimg.imread(os.path.join(input_folder, img_name[:-4]+'.jpg'))
imgplot = axs[0].imshow(img)
axs[0].plot(ldmrks[:,0],ldmrks[:,1],'.r', markersize=mks)
axs[0].axis('off')

centers = [item for item in model['center']]
axs[1].plot(np.array(centers).squeeze()[:,0],np.array(centers).squeeze()[:,1],'.m', markersize=7, label='posterior means')
axs[1].plot(ldmrks_a[:,0],ldmrks_a[:,1],'go', markersize=mks, label='aligned points')
for center, rotation, values in zip(centers, eigen_vectors, eigen_values):
	if center is not None and rotation is not None and values is not None: 
            x,y,z = build_pca_ellipsoid(np.array(center), np.array(rotation), np.array(values))

            xa, ya = [], []
            
            for i in range(x.shape[0]**2):
                xa.append(x.item(i))
                ya.append(y.item(i))
                
            points = np.asarray([xa,ya]).T
            hull = ConvexHull(points)
            for simplex in hull.simplices:
                axs[1].plot(points[simplex, 0], points[simplex, 1], 'c-', linewidth = 0.5)

axs[1].axis('off')
plt.legend(prop={'size': 18})

plt.savefig(os.path.join(save_folder, img_name[:-4]+'_'+str(fa_method)+'.pdf'), dpi=100)
