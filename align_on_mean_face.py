#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 15:26:42 2020

@authors: Mostafa SADEGHI, Sylvain GUY

"""

"""
This code aligns landmarks extracted by different methods on the mean face landmarks

"""

import sys
sys.path.insert(0, "./")
import os
import numpy as np
from tools.libAlign import robustGumRegSimplified,alignQuatCF,robust_Student_reg,alignCf
from multiprocessing import Pool
import scipy.io as sio

def alignAFace(pathToImage):
    
    fa = '2DASL' 
    method = 'gum' # student or gum
    pathToModel = './mean_face/mean_face_new.txt'
    path, fname = os.path.split(pathToImage)
    

    if fa == 'bulat':
        pathOut = os.path.join(main_dir, 'aligned_ldks_bulat')
        face = np.loadtxt(pathToImage).T 
    elif fa == 'prnet':
        pathOut = os.path.join(main_dir,'aligned_ldks_PRNET')
        face = np.loadtxt(pathToImage).T 
    elif fa == 'gt':
        pathOut = os.path.join(main_dir,'aligned_ldks_GT')
        face = np.float64(sio.loadmat(pathToImage)['pt3d_68'])
    elif fa == '3ddfa':
        pathOut = os.path.join(main_dir,'aligned_ldks_3DDFA')
        face = np.loadtxt(pathToImage)
    elif fa == '2DASL':
        pathOut = os.path.join(main_dir,'aligned_ldks_2DASL')
        face = np.loadtxt(pathToImage)
            
    Y = np.loadtxt(pathToModel).T
    idcs = np.arange(0,68)
    if data_set== '3DFAW':
        Y = np.delete(Y, [60,64], axis=1)
        idcs = np.arange(0,66)
        
    maxiter = 100
    
    
    R0, t0, s0, reason_crash = alignQuatCF(Y,face) 
    
    if face.size ==0:
        return
    
    if method=="gum":
        R, s, Sig, t, _, _, _, _, _, reason_crash, errs = robustGumRegSimplified(Y[:,idcs],face[:,idcs],R0,s0,t0,maxiter, None, amplitudeUniform=0.5,pi=0.8)
        if reason_crash is not None:
            print("CRASH! " + reason_crash)
            R = R0
            s = s0
            t = t0
            errs = None
        else:
            errs = errs.tolist()
    elif method=="student":
        R, s, Sig, t, w, _, _, _, reason_crash  = robust_Student_reg(Y[:,idcs], face[:,idcs], R0, s0, t0, maxiter, None)
        if reason_crash is not None:
            print("CRASH! " + reason_crash)
            R = R0
            s = s0
            t = t0
 
        errs = w.tolist()
    elif method=="gau":
        R, t, s, proj = alignCf(face[:,idcs],Y[:,idcs],maxiter,False)
        errs            = np.ones((1,68))
    else:
        raise Exception        
        
    Ya = s*R.dot(face) + t
    
    if fa == 'bulat':
        np.savetxt(os.path.join(pathOut, fname[:-8]+'_'+str(method)+'.txt'),Ya.T)
    elif fa == 'prnet':
        np.savetxt(os.path.join(pathOut, fname[:-4]+'_'+str(method)+'.txt'),Ya.T)
    elif fa == 'gt':
        np.savetxt(os.path.join(pathOut, fname[:-4]+'_'+str(method)+'.txt'),Ya.T)
    elif fa == '3ddfa':
        np.savetxt(os.path.join(pathOut, fname[:-4]+'_'+str(method)+'.txt'),Ya.T)
    elif fa == '2DASL':
        np.savetxt(os.path.join(pathOut, fname[:-4]+'_'+str(method)+'.txt'),Ya.T)            
        
    return

input_folder = './AFLW2000/ldks_bulat/'
main_dir = './AFLW2000/'

data_set = 'AFLW2000'

file_list = [os.path.join(root, name)
     for root, dirs, files in os.walk(input_folder)
     for name in files
     if name.endswith('.txt')]  
       
args = []
  
with Pool(None) as p:
    p.map(alignAFace, file_list)