# Performance Analysis of 3D Face Alignment with a Statistically Robust Confidence Test

This repository contains codes and data for the method proposed in [1].

The neutral frontal landmark model can be found in the folder `./mean_face`.

Four statistical frontal landmark models can be found in the folder `./statistical_frontal_landmark_models`. These models have been built by using two face alignment methods, 3DFA1 [2] and 3DFA2 [3] in conjuction with two robust algorithms to find a rigid transformation:

GUM-EM (Gaussian plus uniform mixture model) and GStudent-EM (Student's t-distribution). For more details, please refer to [1].

The five 3DFA algorithms considered in this work are: Bulat [2], PRNet [3], 3DDFA [4], 2DASL [5], and 3DDFA-V2 [6]. The public implementations of these methods can be found below:

* **face alignment** (Bulat's method): https://github.com/1adrianb/face-alignment
* **PRNet**: https://github.com/YadiraF/PRNet
* **3DDFA**: https://github.com/cleardusk/3DDFA
* **2DASL**: https://github.com/XgTu/2DASL
* **3DDFA-V2**: https://github.com/cleardusk/3DDFA_V2

The scripts in this repository are described as follows:


* `align_on_mean_face.py`: to align localized landmarks by each one of the 3DFA algorithms as well as the ground truth annotations on the mean face.

* `show_statistical_face_model.py`: to show each one of four different statistical landmark models.

* `NME_AUC_CED_computations.ipynb`: to compute supervised performance measures (NME, AUC, and CED) between localized landmarks and the ground truth ones.

* `Supervised_Unsupervised_performance_analysis.ipynb`: to compute the unsupervised (proposed) measure and compared it with the supervised measures, in terms of correlation.

* `show_example_face.py`: to show some example face images with aligned landmarks and the statistical landmarks.



**References**

[1] M. Sadeghi, X. Alameda-Pineda, R. Horaud, _[Performance Analysis of 3D Face Alignment with a Statistically Robust Confidence Test](https://team.inria.fr/robotlearn/upa3dfa/)_, Submitted to a journal.

[2] A. Bulat, G. Tzimiropoulos, _How far are we from solving the 2D \& 3D Face Alignment problem? (and a dataset of 230,000 3D facial landmarks)_, In International Conference on Computer Vision, pp. 1021-1030, 2017.

[3] Y. Feng, F. Wu, X. Shao, Y. Wang, X. Zhou, _Joint 3D face reconstruction and dense alignment with position map regression network_, In European Conference on Computer Vision, pp. 534–55, 2018.

[4] X. Zhu, Z. Lei, X. Liu, H. Shi, SZ. Li, _Face alignment across large poses: a 3D solution_, In IEEE Conference on Computer Vision and Pattern Recognition, pp. 146–155, 2016.

[5] M. Tu, J. Zhao, Z. Jiang, Y. Luo, M. Xie, Y. Zhao, L. He, Z. Ma, J. Feng, _3D face reconstruction from a single image assisted by 2D face images in the wild_, IEEE Transactions on Multimedia, pp. 1160-1172, 2020.

[6] J. Guo, X. Zhu, Y. Yang, F. Yang, Z. Lei, and S. Z. Li, _Towards fast, accurate and stable 3D dense face alignment_, in European Conference
on Computer Vision, pp. 152–168, 2020.
