#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 19:10:04 2020

@authors: Mostafa SADEGHI, Sylvain GUY

"""

"""
This code displays 4 different statistical frontal landmark models. 

"""
import os
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
from scipy.spatial import ConvexHull
import json
import numpy as np
import matplotlib.pyplot as plt

from performance_model import build_pca_ellipsoid

from six.moves import cPickle as pickle #for performance

def save_dict(di_, filename_):
    with open(filename_, 'wb') as f:
        pickle.dump(di_, f)

def load_dict(filename_):
    with open(filename_, 'rb') as f:
        ret_di = pickle.load(f)
    return ret_di


#%% Show some examples:

save_fig = 1

save_folder = './results'
models_path = './statistical_frontal_landmark_models/'

# 4 different statistical landmark models:
model_pool = ['3DFA1_GStudent_EM.txt', '3DFA1_GUM_EM.txt', '3DFA2_GStudent_EM.txt', '3DFA2_GUM_EM.txt']

# which model to use
model_id = 0

pathModel = os.path.join(models_path, model_pool[model_id])

with open(pathModel) as json_file:  
    model = json.load(json_file)

mks = 7

ax = plt.subplot(111)
eigen_vectors = [item for item in model['eigen_vectors']]
centers = [item for item in model['center']]
eigen_values = [item for item in model['eigen_values']]
plt.plot(np.array(centers).squeeze()[:,0],np.array(centers).squeeze()[:,1],'.k', markersize=5) #, label='mean face')
   
ax.set_axis_off()
for center, rotation, values in zip(centers, eigen_vectors, eigen_values):
	if center is not None and rotation is not None and values is not None: 
            x,y,z = build_pca_ellipsoid(np.array(center), np.array(rotation), np.array(values))

            xa, ya = [], []
            
            for i in range(x.shape[0]**2):
                xa.append(x.item(i))
                ya.append(y.item(i))
                
            points = np.asarray([xa,ya]).T
            hull = ConvexHull(points)
            for simplex in hull.simplices:
                plt.plot(points[simplex, 0], points[simplex, 1], 'm-', linewidth = 0.5)

            
plt.legend()
plt.title('Statistical frontal landmark model')
plt.axis('equal')


if save_fig:
    plt.savefig(os.path.join(save_folder, model_pool[model_id][:-4]+'.pdf'), dpi=300)
